import express = require('express');
import {Request, Response} from 'express';
import {User} from './User';
import {UserList} from "../../../AB0/src/classes";
//import {User} from '../../../AB0/src/classes'
let userList: User[] = [];
const app = express();
app.use(express.json());
app.listen(8080, () => {
    console.log('Server started at http://localhost:8080');
});

/**
 * @api {post} /user Create a new user
 * @apiName PostUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {string} firstName The first name of the user to create
 * @apiParam {string} lastName The last name of the user to create
 *
 * @apiSuccess {User} user The created user object
 * @apiSuccess {string} message Message stating that the user has been created
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 201 Created
 *  {
 *      "user":{
 *          "id":1,
 *          "firstName":"Peter",
 *          "lastName":"Kneisel"
 *      },
 *      "message":"User created"
 *  }
 */
app.post('/user', (req: Request, res: Response) => {
    // Read data from request body
    const firstName: string = req.body.firstName;
    const lastName: string = req.body.lastName;
    const userId: number = User.idCounter;
    //user: string = req.body, => user.firstName && user.lastName
    if (firstName == "" && lastName == "") {
        res.status(500).send({
            message: "No User added"
        })
    } else {
        const user: User = new User(firstName, lastName, userId);
        userList.push(user);
        res.status(201).send({
            user: user,
            message: "New User created"
        });
    }
    /**
     if (firstName && lastName) {

	 	const user: User = new User(firstName, lastName);
	 	userList.addUser(user);

	 	res.status(201).send({
			message: 'New User created'
		});
	 } else {
	 	res.status(400).send({
	 		message: 'Fields not filled'
	 	});
	 }
     **/


    // TODO Implement this route

    // TODO When creating a new user, the id does not have to be supplied. It will be generated automatically

});

/**
 * @api {get} /user/:userId Request specified user
 * @apiName GetUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {number} userId The id of the user to fetch
 *
 * @apiSuccess {User} user The requested user object
 * @apiSuccess {string} message Message stating the requested user has been fetched
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 201 Created
 *  {
 *      "user":{
 *          "id":1,
 *          "firstName":"Peter",
 *          "lastName":"Kneisel"
 *      },
 *      "message":"User fetched"
 *  }
 *
 *  @apiError (Client Error) {404} UserNotFound The requested user has not been found
 *
 *  @apiErrorExample UserNotFound:
 *  HTTP/1.1 404 Not Found
 *   {
		"message":"User not found"
	}
 */
app.get('/user/:userId', (req: Request, res: Response) => {
    // Read data from request parameters
    const userId: number = req.params.userId;
    // Search user in user list
    for (const user of userList) {
        if (user.id == userId) {
            res.status(200).send({
                user: user,
                message: 'User fetched'
            });
            // Terminate this route
            return;
        }
    }
    // The requested user was not found
    res.status(404).send({
        message: 'User not found'
    });
});


/**
 * @api {put} /user/:userId Update specified user
 * @apiName PutUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {number} userId The id of the user to update
 * @apiParam {string} firstName The first name of the user to update
 * @apiParam {string} lastName The last name of the user to update
 *
 * @apiSuccess {User} user The updated user object
 * @apiSuccess {string} message Message stating the requested user has been fetched
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "user":{
 *          "id":1,
 *          "firstName":"Peter",
 *          "lastName":"Kneisel"
 *      },
 *      "message":"User updated"
 *  }
 *
 *  @apiError (Client Error) {404} UserNotFound The requested user has not been found
 *
 *  @apiErrorExample UserNotFound:
 *  HTTP/1.1 404 Not Found
 *   {
		"message":"User not found"
	}
 */
// TODO Implement this route


app.put('/user/:userId', (req: Request, res: Response) => {
    const userId: number = req.params.userId;
    const firstName: string = req.body.firstName;
    const lastName: string = req.body.lastName;
    for (const user of userList) {
        if (user.id == userId) {
            res.status(200).send({
                user: user,
                firstName: firstName,
                lastName: lastName,
                message: 'User updated'
            });
            return
        }
    }
    res.status(404).send({
        message: 'User not found'
    });
})


/**
 * @api {delete} /user/:userId Delete specified user
 * @apiName DeleteUser
 * @apiGroup User
 * @apiVersion 1.0.0
 *
 * @apiParam {number} userId The id of the user to delete
 *
 * @apiSuccess {string} message Message stating the requested user has been deleted
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "message":"User deleted"
 *  }
 *
 *  @apiError (Client Error) {404} UserNotFound The requested user has not been found
 *
 *  @apiErrorExample UserNotFound:
 *  HTTP/1.1 404 Not Found
 *   {
		"message":"User not found"
	}
 */
app.delete('/user/:userId', (req: Request, res: Response) => {
    const userId: number = req.params.userId;
    // Search user in user list
    for (const user of userList) {
        if (user.id == userId) {
            for (let i = 0; i < userList.length; i++)
                if (userList[i].id === user.id) {
                    userList.splice(i, 1);
                    break;
                }
                res.status(200).send({
                    user: user,
                    message: 'User deleted'
                });
                return;
        }
    }
    // The requested user was not found
    res.status(404).send({
        userList,
        message: 'User not found'
    })
})


/**
 * @api {get} /users Get all users
 * @apiName GetUsers
 * @apiGroup Users
 * @apiVersion 1.0.0
 *
 * @apiSuccess {User[]} userList The list of all users
 * @apiSuccess {string} message Message stating the requested user has been fetched
 *
 * @apiSuccessExample Success-Response:
 *  HTTP/1.1 200 OK
 *  {
 *      "userList":[
 *          {
 *              "id":1,
 *              "firstName":"Peter",
 *              "lastName":"Kneisel"
 *          },
 *          ...
 *      ],
 *      "message":"List of all users fetched"
 *  }
 */
app.get('/users', (req: Request, res: Response) => {
    // Search user in user list
    if (userList.length >= 1) {
        res.status(200).send({
            userList,
            message: 'List of all users fetched'
        });
    } else {
        // The requested user was not found
        res.status(200).send({
            userList,
            message: 'Users not found'
        });
    }
});

export {app, userList}; // For testing purposes. DO NOT DELETE
