/*****************************************************************************
 * Import package                                                            *
 *****************************************************************************/
import express = require ('express');
import {Request, Response} from 'express';
import {User, UserList, Group} from '../model/user';
import session = require ("express-session");
import cryptoJS = require("crypto-js")
import mysql = require("mysql");
import {Connection, MysqlError} from "mysql";

const app = express();


// DB-Config
const database: Connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'userman'
})

/*****************************************************************************
 * Define global variables                                                   *
 *****************************************************************************/
let userList: UserList = new UserList();

/*****************************************************************************
 * Session / Cookies                                                *
 *****************************************************************************/
//userList[0] = new User(6, 'Jan Ole', 'Schmidt', '3:28:59 PM','webp2', 'admin');
//userList[1] = new User(' ', ' ',' ', ' ',' ', ' ');

function checkPassword(username: string, password: string): boolean {
    for (let i in userList) {
        if (username == userList[i].username && password == userList[i].password) {
            return true;
        }
    }
    return false;
}

declare module 'express-session' {
    interface SessionData {
        username: string,
        password: string
    }
}

/*****************************************************************************
 * Define and start web-app server, define json-Parser                       *
 *****************************************************************************/
app.listen(8080, () => {
    console.log('Server started: http://localhost:8080');

    // DB-Connection
    database.connect((err: MysqlError) => {
        if (err) {
            console.log('Database connection failed: ', err);
        } else {
            console.log('Database is connected');
        }
    });
});

/***************************
 * Session Management configuration                                          *
 ***************************/

app.use((session({
    // Save session even if not modified
    resave: true,
    // Save session even if not used
    saveUninitialized: true,
    // Forces cookie set on every response needed to set expiration (maxAge)
    rolling: true,
    // Encrypt session-id in cookie using "secret" as modifier
    secret: 'geheim',
    // Name of the cookie set is set by the server
    name: 'mySessionCookie',
    // Set some cookie-attributes. Here expiration-date (offset in ms)
    cookie: {maxAge: 60 * 1000}
})));

app.use(express.json());


/***************************
 * Session Management configuration                                          *
 ***************************/

//not fixed
function isLoggedIn() {
    return (req: Request, res: Response, next) => {
        if (req.session.username) {
            next();
        } else {
            res.status(401).send({
                message: 'Session expired, please log in again.',
            });
        }
    };
}

/*****************************************************************************
 * STATIC ROUTES                                                             *
 *****************************************************************************/
const basedir: string = __dirname + '/../..';  // get rid of /server/src
app.use('/', express.static(basedir + '/client/views'));
app.use('/css', express.static(basedir + '/client/css'));
app.use('/src', express.static(basedir + '/client/src'));
app.use('/jquery', express.static(basedir + '/client/node_modules/jquery/dist'));
app.use('/popperjs', express.static(basedir + '/client/node_modules/popper.js/dist'));
app.use('/bootstrap', express.static(basedir + '/client/node_modules/bootstrap/dist'));
app.use('/font-awesome', express.static(basedir + '/client/node_modules/font-awesome'));

/*****************************************************************************
 * HTTP ROUTES: USER, USERS                                                  *
 *****************************************************************************/
/**
 * @api {post} /user Create a new user
 * @apiName postUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {string} firstName First name of the user
 * @apiParam {string} lastName Last name of the user
 *
 * @apiSuccess {string} message Message stating the new user has been created successfully
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully created new user"
 * }
 *
 * @apiError (Client Error) {400} NotAllMandatoryFields The request did not contain all mandatory fields
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 400 Bad Request
 * {
 *     "message":"Not all mandatory fields are filled in"
 * }
 */

app.post('/user', (req: Request, res: Response) => {
    // Read data from request body
    const firstName: string = req.body.firstName;
    const lastName: string = req.body.lastName;
    const userGroups: Group[] = req.body.userGroups

    // add a new user if first- and lastname exist
    if (firstName && lastName) {
        //data array
        let data: [string, string, string] = [
            new Date().toLocaleTimeString(),
            firstName,
            lastName,
        ];
        //define DB-request
        let query: string = 'INSERT INTO userlist (creationTime, firstName, lastName) VALUES (?, ?, ?);';

        database.query(query, data, (innerErr: MysqlError, result: any) => {
            if (innerErr) {
                res.status(400).send({
                    message: 'Database request failed'
                })
            } else {
                const innerQuery: string = 'INSERT INTO user_group (user_id, group_id) VALUES ?;';
                const innerData = [];

                for (const group of userGroups) {
                    innerData.push([result.insertId, group]);
                }
                database.query(innerQuery, [innerData], (err: MysqlError) => {
                    if (err || result === null) {
                        res.status(400).send({
                            message: 'Database request failed'
                        });
                    } else {
                        res.status(201).send({
                            message: 'Successfully created new user'
                        })
                    }

                });
            }
        })
    }
})
/**
 * @api {get} /user:userId Get user with given id
 * @apiName getUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 *
 * @apiSuccess {User} user The requested user object
 * @apiSuccess {string} message Message stating the user has been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "user":{
 *         "id":1,
 *         "firstName":"Peter",
 *         "lastName":"Kneisel",
 *         "creationTime":"2018-10-21 14:19:12"
 *     },
 *     "message":"Successfully got user"
 * }
 *
 *  @apiError (Client Error) {404} NotFound The requested user can not be found
 *
 * @apiErrorExample NotFound:
 * HTTP/1.1 404 Not Found
 * {
 *     "message":"The requested user can not be found."
 * }
 */
app.get('/user/:userId', (req: Request, res: Response) => {
    // Create database data and query
    let data: number = Number(req.params.userId);
    let query: string = 'SELECT * FROM userlist WHERE id = ?;';

    database.query(query, data, (err: MysqlError, rows: any) => {
        if (err) {
            // Database operation has failed
            res.status(500).send({
                message: 'Database request failed: ' + err
            });
        } else {
            // Check if database response contains exactly one entry
            if (rows.length === 1) {
                res.status(200).send({
                    message: 'Successfully got user.',
                    user: new User(
                        rows[0].id,
                        rows[0].firstName,
                        rows[0].lastName,
                        new Date(rows[0].creationTime),
                        rows[0].username,
                        rows[0].password
                    )
                });
            } else {
                res.status(404).send({
                    message: 'The requested user can not be found.'
                });
            }
        }
    });
});

/**
 * @api {put} /user/:userId Update user with given id
 * @apiName putUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 * @apiParam {string} firstName The (new) first name of the user
 * @apiParam {string} lastName The (new) last name of the user
 *
 * @apiSuccess {string} message Message stating the user has been updated
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully updated user ..."
 * }
 *
 * @apiError (Client Error) {400} NotAllMandatoryFields The request did not contain all mandatory fields
 * @apiError (Client Error) {404} NotFound The requested user can not be found
 *
 * @apiErrorExample NotAllMandatoryFields:
 * HTTP/1.1 400 Bad Request
 * {
 *     "message":"Not all mandatory fields are filled in"
 * }
 *
 * @apiErrorExample NotFound:
 * HTTP/1.1 404 Not Found
 * {
 *     "message":"The user to update could not be found"
 * }
 */
app.put('/user/:userId', (req: Request, res: Response) => {
    // Read data from request
    const userId: number = Number(req.params.userId);
    const firstName: string = req.body.firstName;
    const lastName: string = req.body.lastName;

    let data: [string, string, number] = [firstName, lastName, userId];
    let query: string = 'UPDATE userlist SET firstName = ?, lastName = ? WHERE id = ?;';

    // Check that all arguments are given
    database.query(query, data, (err: MysqlError, result: any) => {
        if (err) {
            res.status(500).send({
                message: 'Database request failed: ' + err
            });
        } else {
            if (result.affectedRows === 1) {
                res.status(200).send({
                    message: 'Successfully updated user ' + userId
                });
            } else {
                res.status(404).send({
                    message: 'Updating the user failed.'
                });
            }
        }
    })
})

/**
 * @api {delete} /user/:userId Delete user with given id
 * @apiName deleteUser
 * @apiGroup User
 * @apiVersion 2.0.0
 *
 * @apiParam {number} userId The id of the requested user
 *
 * @apiSuccess {string} message Message stating the user has been updated
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *     "message":"Successfully deleted user ..."
 * }
 */
app.delete('/user/:userId', (req: Request, res: Response) => {
    // Read data from request
    const userId: number = Number(req.params.userId);

    let data: number = userId;
    let query: string = "DELETE FROM userlist WHERE id = ?;";

    // delete user
    database.query(query, data, (err: MysqlError, result: any) => {
        if (err) {
            res.status(500).send({
                message: "Database request failed: " + err
            });
        } else {
            if (result.affectedRows === 1) {
                res.status(200).send({
                    message: "Successfully deleted user " + userId
                });
            } else {
                res.status(404).send({
                    message: "The requested user can not be deleted."
                })
            }
        }
    })
});

/**
 * @api {get} /users Get all users
 * @apiName getUsers
 * @apiGroup Users
 * @apiVersion 2.0.0
 *
 * @apiSuccess {User[]} userList The list of all users
 * @apiSuccess {string} message Message stating the users have been found
 *
 * @apiSuccessExample Success-Response:
 * HTTP/1.1 200 OK
 * {
 *    "userList": [
 *      {
 *        "firstName": "Hans",
 *        "lastName": "Mustermann",
 *        "creationTime": "2018-11-04T13:02:44.791Z",
 *        "id": 1
 *     },
 *      {
 *        "firstName": "Bruce",
 *        "lastName": "Wayne",
 *        "creationTime": "2018-11-04T13:03:18.477Z",
 *        "id": 2
 *      }
 *    ]
 *    "message":"Successfully requested user list"
 * }
 */

app.get('/users', (req: Request, res: Response) => {
    // select all elements from database userlist
    let query: string = 'SELECT userlist.id, userlist.firstName, userlist.lastName, groups.id AS "groupId", groups.name AS "groupName"' +
        'FROM userlist JOIN user_group ON userlist.id = user_group.user_id JOIN groups ON user_group.group_id = groups.id ORDER BY userlist.id';

    database.query(query, (err: MysqlError, rows: any) => {
        if (err) {
            res.status(500).send({
                message: "database request failed:" + err
            })
        } else {
            let userlist: User[] = [];
            let lastUser: User;
            // rows => database userlist array; row => user of userlist
            for (const row of rows) {
                if (!lastUser || lastUser.id !== row.id) {
                    lastUser = new User(
                        row.id,
                        row.firstName,
                        row.lastName,
                        new Date(rows.creationTime),
                        row.username,
                        row.password
                    );
                    userlist.push(lastUser);
                }
                lastUser.groups.push(new Group(row.id, row.name))
            }
            res.status(200).send({
                userList: userlist,
                message: 'Successfully requested user list'
            });
        }
    })
});

app.get('/groups', (req: Request, res: Response) => {
    const query: string = 'SELECT * FROM groups';

    database.query(query, (err: MysqlError, rows: any) => {
        if (err) {
            res.status(500).send({
                message: "Database request failed : " + err,
            });
        } else {
            res.status(200).send({
                groups: rows,
                message: "Successfully requested groups list",
            });
        }
    });
});


//Sessions & handleLogin/logout

app.post('/login', (req: Request, res: Response) => {
    let username: string = req.body.username;
    let password: string = req.body.password;
    let query: string = "SELECT * FROM userlist WHERE userlist.username = ? AND userlist.password = ?";

    let data: [string, string] = [
        username,
        password
        //cryptoJS.SHA512(password).toString(),
        // use cryptoJS to have a standard + secure cryptographic algorithm
    ];

    database.query(query, data, (err: MysqlError, rows: any) => {
        // error case
        if (err) {
            res.status(500).send({
                message: 'Database request failed.',
            });
        } else {
            if (rows.length === 1) {
               let user = new User(
                    rows[0].id,
                    rows[0].firstName,
                    rows[0].lastName,
                    new Date(rows[0].creationTime),
                    rows[0].username,
                    rows[0].password
                );
                req.session.username = username;
                res.status(200).send({
                    message: 'Successfully logged in.' + user,
                })
            } else {
                res.status(401).send({
                    message: "Not Valid: user '" + username + "' does not match password",
                    username: username
                })
            }
        }
    });
});

app.post('/logout', (req: Request, res: Response) => {
    delete req.session.username;
    res.status(200).send({
        message: 'Successfully logged out.',
    });
});

app.get('/login/check', isLoggedIn(), (req: Request, res: Response) => {
    if (req.session.username) {
        res.status(200).send({
            message: 'user ' + req.session.username + 'still logged in.',
            username: req.session.username
        });
    } else {
        res.status(401).send({
            message: 'Session is unlogged.',
        })
    }
});

app.post('/btn/:id', (req: Request, res: Response) => {
    let id: string = req.params.id;
    let username: string = req.session.username;
    res.status(200).send({
        message: username + 'klicked on button' + id,
        username: username
    });
});
