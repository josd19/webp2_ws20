-- phpMyAdmin SQL Dump
-- version 5.0.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Erstellungszeit: 05. Dez 2020 um 18:38
-- Server-Version: 10.4.14-MariaDB
-- PHP-Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `userman`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `groups`
--

CREATE TABLE `groups` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `groups`
--

INSERT INTO `groups` (`id`, `name`) VALUES
(1, 'Admin'),
(2, 'User');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `userlist`
--

CREATE TABLE `userlist` (
  `id` int(11) NOT NULL,
  `firstName` varchar(11) NOT NULL,
  `lastName` varchar(11) NOT NULL,
  `creationTime` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `userlist`
--

INSERT INTO `userlist` (`id`, `firstName`, `lastName`, `creationTime`) VALUES
(6, 'Jan Ole', 'Schmidt', '3:28:59 PM'),
(9, 'Selina', 'Kyle', '3:47:13 PM'),
(10, 'awd', 'sef', '3:50:07 PM'),
(12, 'Peter', 'Lustig', '3:50:04 PM'),
(13, 'Peter', 'Kneisel', '3:58:54 PM'),
(14, 'ole', 'schmidt', '5:34:31 PM'),
(15, 'Jan Ole', 'Schmidt', '9:47:23 AM'),
(27, 'aed', 'aed', '4:46:55 PM'),
(29, 'a', 'a', '4:55:20 PM'),
(30, 'a', 'a', '4:55:41 PM'),
(31, 'a', 'a', '4:55:58 PM'),
(32, 'a', 'a', '4:56:17 PM'),
(33, 'b', 'b', '4:56:53 PM'),
(34, 'b', 'b', '4:57:10 PM'),
(35, 'awd', 'aef', '5:08:26 PM'),
(36, 'awd', 'aef', '5:09:49 PM'),
(37, 'awd', 'aef', '5:10:55 PM'),
(38, 'awd', 'aef', '5:12:54 PM'),
(39, 'as', 'adf', '5:38:48 PM'),
(40, 'a', 'a', '5:40:35 PM'),
(41, 'a', 'a', '5:40:44 PM'),
(42, 'a', 'a', '5:41:27 PM'),
(43, 'A', 'A', '5:44:46 PM'),
(44, 'TEST', 'TEST', '6:03:37 PM'),
(45, 'TEST', 'TEST', '6:04:54 PM'),
(46, 'TEST', 'TEST', '6:05:02 PM'),
(47, 'TEST', 'TEST', '6:05:24 PM'),
(48, 'TEST', 'TEST', '6:05:25 PM'),
(49, 'TEST', 'TEST', '6:05:31 PM'),
(50, 'TEST', 'TEST', '6:06:25 PM'),
(51, 'test', 'test', '6:18:04 PM'),
(52, 'Both', 'Groups', '6:37:09 PM');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user_group`
--

CREATE TABLE `user_group` (
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Daten für Tabelle `user_group`
--

INSERT INTO `user_group` (`user_id`, `group_id`) VALUES
(12, 1),
(6, 1),
(9, 2),
(51, 1),
(52, 1),
(52, 2);

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `userlist`
--
ALTER TABLE `userlist`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `user_group`
--
ALTER TABLE `user_group`
  ADD KEY `user_id` (`user_id`) USING BTREE,
  ADD KEY `group_id` (`group_id`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `groups`
--
ALTER TABLE `groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT für Tabelle `userlist`
--
ALTER TABLE `userlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `user_group`
--
ALTER TABLE `user_group`
  ADD CONSTRAINT `user_group_ibfk_2` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `user_group_ibfk_3` FOREIGN KEY (`user_id`) REFERENCES `userlist` (`id`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
