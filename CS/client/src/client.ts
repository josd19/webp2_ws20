/*****************************************************************************
 * interface declaration                                                     *
 *****************************************************************************/

// Interface representing a user
interface User {
    id: number;
    firstName: string;
    lastName: string;
    creationTime: Date;
    password: string;
    username: string;
}

interface Group {
    id: number;
    name: string;
}


/*****************************************************************************
 * Event Handlers (callbacks)                                                *
 *****************************************************************************/
function addUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const addUserForm: JQuery = $('#add-user-form');
    const firstNameField: JQuery = $('#add-first-name-input');
    const lastNameField: JQuery = $('#add-last-name-input');

    // Read values from input fields
    const firstName: string = firstNameField.val().toString().trim();
    const lastName: string = lastNameField.val().toString().trim();
    let userGroups: any =  []

    addUserForm.find('input[type="checkbox"]:checked').each((index , element ) => {
        userGroups.push($(element).val());
    });
    // Check if all required fields are filled in
    if (firstName && lastName) {
        $.ajax({
            url: '/user',
            type: 'POST',
            dataType: 'json',
            data: JSON.stringify({
                firstName,
                lastName,
                userGroups
            }),
            contentType: 'application/json',
            success: (response) => {
                // render Message
                renderMessage(response.message);
                // Update local user list
                getList();
                // Reet the values of all elements in the form
                addUserForm.trigger('reset');

            },
            error: (jqXHRresponse) => {
                renderMessage(jqXHRresponse.responseJSON.message);
            },
        }).then(() => {
        });
    } else {
        // Not all required fields are filled in, print error message
        renderMessage('Not all fields are filled. Please check the form');
    }
}

function editUser(event) {
    // Prevent the default behaviour of the browser (reloading the page)
    event.preventDefault();

    // Define JQuery HTML objects
    const editModal: JQuery = $('#edit-user-modal');
    const editUserForm: JQuery = $('#edit-user-form');
    const firstNameInput: JQuery = $('#edit-first-name-input');
    const lastNameInput: JQuery = $('#edit-last-name-input');
    const idHiddenInput: JQuery = $('#edit-id-input');

    // Read values from input fields
    const userId: number = Number(idHiddenInput.val().toString().trim());
    const firstName: string = firstNameInput.val().toString().trim();
    const lastName: string = lastNameInput.val().toString().trim();
    let userGroups: any =  []


    // Check if all required fields are filled in
    if (firstName && lastName) {
        $.ajax({
            url: '/user/' + userId,
            type: 'PUT',
            dataType: 'json',
            data: JSON.stringify({
                firstName,
                lastName,
                userGroups
            }),
            contentType: 'application/json',
            success: (response) => {
                // render Message
                renderMessage(response.message);
                // Update local user list
                getList();
                // Reset the values of all elements in the form
                editUserForm.trigger('reset');

                 for (const group of response.user.userGroups) {
                    editUserForm.find('input[type="checkbox"][value="${group.id}"]').prop('checked', true);
                }
            },
            error: (jqXHRresponse) => {
                renderMessage(jqXHRresponse.responseJSON.message);
            },
        }).then(() => {
        });
    } else {
        // Not all required fields are filled in, print error message
        renderMessage('Not all fields are filled. Please check the form');
    }
    editModal.modal('hide');
}

function deleteUser(event) {
    // Get user id from button attribute 'data-user-id'
    const userId: number = $(event.currentTarget).data('user-id');

    // Perform ajax request to log out user
    $.ajax({
        url: '/user/' + userId,
        type: 'DELETE',
        dataType: 'json',
        success: (response) => {
            // render Message
            renderMessage(response.message);
            // Get new user list from server
            getList();
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

function openEditUserModal(event) {
    // Get user id from button attribute 'data-user-id'
    const userId: number = $(event.currentTarget).data('user-id');


    $.ajax({
        url: '/user/' + userId,
        type: 'GET',
        dataType: 'json',
        success: (response) => {
            renderEditUserModal(response.user);
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {
    });
}

function getList() {
    // Perform ajax request to update local user list
    $.ajax({
        url: '/users',
        type: 'GET',
        dataType: 'json',
        success: (response) => {
            renderUserList(response.userList);
            //renderUserGroups(response.name, '#user-group-input');
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {});
    $.ajax({
        url: '/groups',
        type: 'GET',
        dataType: 'json',
        success: (response) => {
            renderUserGroups(response.groups, '#user-group-input');
        },
        error: (jqXHRresponse) => {
            renderMessage(jqXHRresponse.responseJSON.message);
        },
    }).then(() => {});
}


/*****************************************************************************
 * Render functions                                                          *
 *****************************************************************************/
function renderMessage(message: string) {
    // Define JQuery HTML Objects
    const messageWindow: JQuery = $('#messages');

    // Create new alert
    const newAlert: JQuery = $(`
        <div class="alert alert-warning alert-dismissible fade show" role="alert">
            ${message}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    `);

    // Add message to DOM
    messageWindow.append(newAlert);

    // Auto-remove message after 5 seconds (5000ms)
    setTimeout(() => {
        newAlert.alert('close');
    }, 5000);
}

function renderUserList(userList: User[]) {
    // Define JQuery HTML objects
    const userTableBody: JQuery = $('#user-table-body');

    // Delete the old table of users from the DOM
    userTableBody.empty();

    // For each user create a row and append it to the user table
    for (const user of userList) {
        // Create html table row element...
        const tableEntry: JQuery = $(`
            <tr>
                <td>${user.id}</td>
                <td>${user.firstName}</td>
                <td>${user.lastName}</td>                
                <td>
                    <button class="btn btn-outline-dark btn-sm edit-user-button mr-4" data-user-id="${user.id}" >
                        <i class="fa fa-pencil" aria-hidden="true"></i>
                    </button>
                    <button class="btn btn-outline-dark btn-sm delete-user-button" data-user-id="${user.id}">
                        <i class="fa fa-trash" aria-hidden="true"></i>
                    </button>
                </td>
            </tr>
        `);
        // ... and append it to the table's body
        userTableBody.append(tableEntry);
    }
}

function renderEditUserModal(user: User) {
    // Define JQuery HTML objects
    const editUserModal: JQuery = $('#edit-user-modal');
    const editIdInput: JQuery = $('#edit-id-input'); // Hidden field for saving the user's id
    const editFirstNameInput: JQuery = $('#edit-first-name-input');
    const editLastNameInput: JQuery = $('#edit-last-name-input');

    // Fill in edit fields in modal
    editIdInput.val(user.id);
    editFirstNameInput.val(user.firstName);
    editLastNameInput.val(user.lastName);

    // Show modal
    editUserModal.modal('show');
}

function renderUserGroups(groups: Group[], destinationSelector: string): void {
    const userGroupContainer: JQuery = $(destinationSelector);
    userGroupContainer.empty();

    for (const group of groups) {
        //console.log(groups)
        userGroupContainer.append(`
                <div class="form-check">
                         <input class="form-check-input " type="checkbox" value="${group.id}"
                                      id="${group.id}" name="groups">
                            <label class ="form-check-label" for="${group.id}">
                                    ${group.name}
                            </label>
                </div>
            `);
    }
}

/*****************************************************************************
 * Session / Cookies                                                         *
 *****************************************************************************/

 function handleLogIn () {
    let data: Object = {username: $('#user-name-input').val(),
                        password: $('#password-input').val()};

    $.ajax({
        url: '/login',
        type: 'POST',
        data: JSON.stringify(data),
        contentType: 'application/json',
        dataType: 'json',
        error: (jqXHR) => {
            showResult(jqXHR.responseJSON.message, jqXHR.status);
        },
        success: (data) => {
            showResult(data.message, 0);
            $('#username').html('user: ' + data.username);
            $('#contentArea').show();
            $('#logIn').hide();
            $('#logOut').show();
            $('#user-name-input').val("");
            $('#password-input').val("");
        },
    }).then(() => {});
}

 function handleLogOut () {
    $('#contentArea').hide();
    $('#logOut').hide();
    $('#logIn').show();

    $.ajax({
        url:            '/logout',
        type:           'POST',
        dataType:       'json',
        headers:        {},
        error:          (jqXHR) => {
            showResult(jqXHR.responseJSON.message, jqXHR.status);},
        success:        (data) => {showResult(data.message,0);},
    }).then(() => {});
}

 function handleBtn() {
    let buttonID: string = this.getAttribute('id');

    $.ajax({
        url:            '/btn/' + buttonID,
        type:           'POST',
        dataType:       'json',
        headers:        {},
        error:          (jqXHR) => {
            showResult(jqXHR.responseJSON.message, jqXHR.status);
            $('#contentArea').hide();
            $('#logout').hide();
            $('#login').show();
       },
       success:         (data) => {
            showResult(data.message, 0); },
    }).then(() => {});
}

 function checkLogIn() {

    $.ajax({
        url:        '/login/check',
        type:       'GET',
        dataType:   'json',
        error:      () => {},
        success:    (data) => {
            $('#username').html('user: ' + data.username);
            $('#contentArea').show();
            $('#logIn').hide();
            $('#logOut').show();
            showResult(data.message,0);
        },
    }).then(() =>{});
}

 let resultTimers: number[] = [];

 function hideAfter(seconds: number, domElement: JQuery) {}
 function showResult(text:string, status: number) {

}


/*****************************************************************************
 * Main Callback: Wait for DOM to be fully loaded                            *
 *****************************************************************************/
$(() => {
    // Define JQuery HTML objects
    const addUserForm: JQuery = $('#add-user-form');
    const editUserForm: JQuery = $('#edit-user-form');
    const userTableBody: JQuery = $('#user-table-body');

    // Register listeners
    addUserForm.on('submit', addUser);// Pass the event into the handler
    editUserForm.on('submit', editUser); // Pass the event into the handler
    userTableBody.on('click', '.edit-user-button', openEditUserModal); // Click listener for edit button
    userTableBody.on('click', '.delete-user-button', deleteUser); // Click listener for delete button

    //some functions
    checkLogIn();
    getList();


     //callbacks for Session/Coockies
     $('#login-btn').on('click', handleLogIn);
     $('#user-name-input, #password-input').on('keyup', function(event:JQuery.Event) {
        if (event.key === 'Enter') {handleLogIn(); // only if "enter"-key is pressed
        }});
     $('#logout-btn').on('click', handleLogOut);

     $('#BTN').on('click' , handleBtn);


});

