/*****************************************************************************
 * Import package                                                            *
 *****************************************************************************/
import express = require ('express');
import { Request, Response } from 'express';

/*****************************************************************************
 * MySQL                                                   *
 *****************************************************************************/

// import of DB-Module
import mysql = require("mysql");                        // <-- handles database connections
import {Connection, MysqlError} from "mysql";

 // DB-Config
const database : Connection = mysql.createConnection({
    host        :   'localhost',
    user        :   'root',
    password    :   '',
    database    :   'userman'
})


/*****************************************************************************
 * Define global variables                                                   *
 *****************************************************************************/
let result: number = random(1,10);
const masterpassword: string = "webp2";

/*****************************************************************************
 * Define and start web-app server, define json-Parser                       *
 *****************************************************************************/
const app = express();
// 8880 if 8080 is loaded/occupied
app.listen(8880, () => {
    console.log('Server started: http://localhost:8080');

    // DB-Connection
    database.connect((err: MysqlError) => {
        if (err) {
            console.log('Database connection failed: ', err);
        } else {
            console.log('Database is connected');
        }
    });
});
app.use(express.json());

/*****************************************************************************
 * STATIC ROUTES                                                             *
 *****************************************************************************/
const basedir: string = __dirname + '/../..';
app.use('/', express.static(basedir + '/client/views'));
app.use('/css', express.static(basedir + '/client/css'));
app.use('/src', express.static(basedir + '/client/src'));
app.use('/jquery', express.static(basedir + '/client/node_modules/jquery/dist'));
app.use('/popperjs', express.static(basedir + '/client/node_modules/popper.js/dist'));
app.use('/bootstrap', express.static(basedir + '/client/node_modules/bootstrap/dist'));
app.use('/@fortawesome', express.static(basedir + '/client/node_modules/@fortawesome/fontawesome-free'));

/*****************************************************************************
 * HTTP ROUTES: GUESS, CHEAT, RESET                                          *
 *****************************************************************************/

function random(min: number, max: number): number {
    return Math.floor(Math.random() * (max - min) + min);
}

//guess-route for finding result
app.get('/guess/:guess', function(req: Request, res: Response) {
    let guess: number = Number(req.params.guess);

    //if the guess-entry is not a number status 400, win: false
    if (isNaN(guess)) {
        res.status(400).json({
            message: 'Please guess a number!',
            win: false
        });
        res.send('Please guess a number!');
        //if guess are highter status 200, win: false
    } else if (guess > result) {
        res.status(200).json({
            message: 'Your guess was too high!',
            win: false
        });
        //if guess is lower status 200, win: false
    } else if (guess < result) {
        res.status(200).json({
            message: 'Your guess was too low!',
            win: false
        });
        //else guess mathched with result status 200, win: true
    } else {
        res.status(200).json({
            message: 'You got it!',
            win: true
        });
    }
});

//cheat-route will return the result if the passwort "webp2" is correct
app.post("/cheat", function (req: Request, res: Response) {

    //const pw to get the password from form entry
    const pw: string = req.body.passwordData;

    //if pw matches the masterpassword the result will returnes
    if (pw == masterpassword) {
        res.status(201);
        res.send(result.toString());
    }
    //else error message
    else {
        res.status(400);
        res.send("Password not ok!");
    }
});

//reset-route will change the ramdom function (min,max = 1,10) to form entries
app.post("/reset", (req: Request, res: Response) => {
    let newMin = req.body.minimum;
    let newMax = req.body.maximum;
    let newresult: number = random(newMin,newMax);

    //if entries are not a number error message appers
    if(isNaN(newMin || newMax)) {
        res.status(400).json({
            message: 'Please guess a number!',
        });

        // else => result/random function changes to newNumber / form entries
    }  else {
        result = newresult;
        res.status(201).json ({
            message: 'Numbers changed!',
        });
    }
});



