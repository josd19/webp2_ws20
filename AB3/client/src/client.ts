/*****************************************************************************
 * Event Handlers (callbacks)                                                *
 *****************************************************************************/

//guess number callback-function targets the a.g.b.-btn
$( () => {
    $('#add-guess-number-btn').on('click', () => {
        event.preventDefault();

        //constant jsonData to initialize guess-number value
        const jsonData: number = Number($('#add-guess-number').val());

        //ajax request to interact with server.ts
        $.ajax({
            url: 'http://localhost:8080/guess/' + jsonData,
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            //appends result to <p> in index.html if successfully
            success:(response) => {
                $('#guess-result').html('')
                $('#guess-result').html(response.message)
            },
            //error if not
            error: (jqXHRresponse,Status, error) => {
                alert(error)
            }
        });
    });

    //cheat callback-function targets the add-passwort button and send the password-value to server.ts
    $('#add-password-btn').on('click', () => {
        event.preventDefault();

        //constant to get value from add-passwort-input
        const passwordData: string = ($('#add-password').val().toString());

        //ajax request to interact with server.ts
        $.ajax({
            url:'http://localhost:8080/cheat',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            //transfer the password value to server.ts
            data: JSON.stringify({passwordData}),
            //appends result to <p> in index.html if successfully
            success:(response) => {
                $('#password-result').html(response)
            },
            //error if not
            error: (jqXHR, Status, error) => {
                alert(error)
            }
        });
    });

    //reset callback-function targets add-min/max btn for changing the min/max values
    $('#add-min-max-number-btn').on('click', () => {
        event.preventDefault();

        //constants for new min/max values
        const minimum: number = Number($('#add-min-val').val());
        const maximum: number = Number($('#add-max-val').val());

        //ajax request to interact with server.ts
        $.ajax({
            url: 'http://localhost:8080/reset',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            //transfer the new min/max value to server.ts
            data: JSON.stringify({minimum,maximum}),
            //appends result to <p> in index.html if successfully
            success: (response) => {
                $('#guess-number-return').html(response.message)
            },
            //error if not
            error: (jqXHR, Status, error) => {
                alert(error)
            }
        });
    });
});