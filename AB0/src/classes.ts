//--- Class Animal
export class Animal {
	public animalName: string;
	public year: number;
	public gender: string;

	constructor(animalName: string, year: number, gender: string) {
		this.animalName = animalName;
		this.year = year;
		this.gender = gender;
	}
}
//--- Class representing a user
export class User {
	private static userCounter: number = 1;  // unique user id
	public id: number;
	public firstName: string;
	public lastName: string;
	public creationTime: Date;
	public pet: Animal;

	constructor(firstName: string, lastName: string, animalName: string, year: number, gender: string, creationTime: Date) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.creationTime = creationTime;
		this.id = User.userCounter++;
		this.pet = new Animal(animalName, year, gender)
	}
}

//--- Class representing a user list
export class UserList {
	private users: User[]; // using array to store user list
	private animals: Animal[];

	constructor() {
		this.users = [];
	}

	getUsers(): User[] {
		return this.users;
	} //gibt alle User zurück

	addUser(user: User) {
		this.users.push(user);
	}

	addAnimal(animal: Animal) { this.animals.push(animal); }


	deleteUser(userId: number): boolean {
		let found: boolean = false;
		// Create a new array without the user to delete
		this.users = this.users.filter((user: User): boolean => {
			if (user.id === userId) {
				found = true;
				return false; // not to be copied
			} else {
				return true; // to be copied
			}
		});
		return found;
	}

	deleteAnimal(userId: number): boolean {
		let found: boolean = false;
		// Create a new array without the user to delete
		this.users = this.users.filter((user: User): boolean => {
			if (user.id === userId) {
				found = true;
				return false; // not to be copied
			} else {
				return true; // to be copied
			}
		});
		return found;
	}

	getUser(userId: number): User {
		// iterate through userList until user found (or end of list)
		for (const user of this.users) {
			if (userId === user.id) {
				return (user);  // leave function with "user" when found
			}
		}
		return null; // leave function with "null" when not found
	}

	editUser(userId: number, firstName: string, lastName: string): boolean {
		// iterate through userList until user found (or end of list)
		for (const user of this.users) {
			if (user.id === userId) {
				user.firstName = firstName;
				user.lastName = lastName;
			//	user.pet.animalName =
				return true;  // leave function with "true" when found
			}
		}
		return false  // leave function with "False" when found
	}

}



